#!/usr/bin/python
# -*- coding: utf-8 -*-

import threading
import time
import paho.mqtt.client as mqtt


def on_connect(
    client,
    userdata,
    flags,
    rc,
    ):
    print 'Connected with result code ' + str(rc)
    client.subscribe('test/#')


def DAS_puller():
    print threading.currentThread().getName(), 'Starting'
    time.sleep(2)
    client = mqtt.Client()
    client.on_connect = on_connect
    client.connect('localhost', 1883, 60)
    client.loop_start()
    while True:
        time.sleep(2)
        client.publish('test/temperature', 'test')


def on_message(client, userdata, msg):
    print msg.topic + ' ' + str(msg.payload)


def DAS_listener():
    print threading.currentThread().getName(), 'Starting'
    time.sleep(3)
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect('localhost', 1883, 60)
    client.loop_forever()


t = threading.Thread(name='DAS_puller', target=DAS_puller)
w = threading.Thread(name='DAS_listener', target=DAS_listener)

w.start()
t.start()
