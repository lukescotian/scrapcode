package java_output_formatting;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class solution {

    public static void main(String[] args) {
        //creating File instance to reference text file in Java
        File text = new File("tests/input/input00.txt");
      
        //Creating Scanner instnace to read File in Java
        try {
			Scanner scnr = new Scanner(text);
	        int lineNumber = 1;
	        while(scnr.hasNextLine()){
	            String line = scnr.nextLine();
	            System.out.println(line);
	            lineNumber++;
	        }  
	        scnr.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   
    }
}
