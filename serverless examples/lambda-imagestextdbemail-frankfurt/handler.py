from model import *


from business_logic_layer.databasebroker import not_currently_indatabase, persist_usermodel
from business_logic_layer.ses_broker import start_ses_system
from business_logic_layer.cache_broker import configure_user_cacheobj, parse_input_cache


def run(event, context):
	client = boto3.client('s3')
	client.put_object(Body=json.dumps(event), Bucket='usercache', Key='@u2sers/@email/general.json')

	# client.put_object(Body=JSON.stringify(usercache_obj), Bucket='usercache', Key='@users/@email/general.json')
	(keys, event, unique_strings) = parse_input_cache(event)
	print keys
	print "Got here"

	for email in keys:
		# delegate emails to a nother microservice and database to an internal module
		if email and email is not_currently_indatabase({"text":'placeholder','email':email}):
			print "Email exists and is not in the database save here"
		else :
			print "Email does not exist and in not in the database"
	# 		# persist_usermodel(email, _json)
	# 		# start_ses_system(_json) #for the template ids passed in by the user
	# 		# usercache_obj = configure_user_cacheobj()
	# 		# client.put_object(Body=JSON.stringify(usercache_obj), Bucket='usercache', Key='@users/@email/general.json')


	# client.put_object(Body=json.dumps(event), Bucket='usercache', Key='@auser/@email/general.json')
	return {
		"message":"message"
	}
