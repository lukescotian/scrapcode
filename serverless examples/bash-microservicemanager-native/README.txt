
The goal of this script is to provide a local environment available to any new linux and git machine. 
The operating system of that machine is responsible for packaging and deploying microservices using the 
sls deploy 
sls syncToS3

and the regular s3 commands. 

The operating system therefore needs a simplified initial deployment and development environment 
provided the parameters provided.


Steps to clone the microrepository-list from the os bash microservice-manager

1: first call into the main.sh script

2: then call in bash copy_repos.sh which is the broker 
	from the bash to an auth repository list 

3: call into the script that will get all authenticated 
	repo names and put it in a human readable file

4: 