
#/bin/bash
mkdir dev 

rm -rf tmp 

mkdir tmp
python setup.py


# # parse the git hub config

url=$(<tmp/url.txt)

tests=$(<tmp/tests.txt)
packages=$(<tmp/packages.txt)

# # clean the cache
# rm -rf tmp

# # then get the username and password
read -r -p 'Please enter your bitbucket username >>> ' username
read -r -s -p 'Please enter your bitbucket password >>> ' password

cd tmp 
cp resources.txt ../dev/
cd ..
cd dev

for repo in $( cat "resources.txt" ); do

    echo item: $repo
    echo Acquiring microservice $repo...
 	
    # clone the production repo
 	git clone https://$username:$password@$url/$username/$repo.git  > /dev/null
 	cd $repo
 	git remote remove origin
 	git remote add origin https://$username@$url/$username/$repo.git  > /dev/null
 	cd ..

 	# clone the test repo
	git clone https://$username:$password@$url/$username/$repo-test.git  > /dev/null
 	cd $repo
 	git remote remove origin
 	git remote add origin https://$username@$url/$username/$repo-test.git  > /dev/null
 	cd ..
   	echo item: $repo acquired
   	echo ----------------------------------
   	# cd ..
    done

cd ..

rm -rf tmp





# then iterate a file with curl
# for reponame in registered-repos

# 	if git_repo_exists 
	

# 	git clone https://<username>:<password>@<url>/<username>/<reponame>.git
# 	//example https://lukescotian:47hjksR123@bitbucket.org/lukescotian/lambda-scrapestockprices-frankfurt.git
# 	# <url>=bitbucket.org

# 	cd <reponame>
	
# 	git remote remove origin
	
# 	unzip main.zip
	
# 	npm install #may fail who cares
# 	pip install #may fail who cares

# 	git add origin https://<username>@<url>/<username>/<reponame>.git

