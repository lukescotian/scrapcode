import json
def get_input_dynamic_configuration_file():
	return json.loads(open("config_dynamic.json","r").read())

def split_configuration():
	config = get_input_dynamic_configuration_file()
	open("tmp/url.txt","w").write(config["url"])
	open("tmp/packages.txt","w").write(config["packages"])
	open("tmp/tests.txt","w").write(config["tests"])
	resources = open("tmp/resources.txt","w")
	for el in config["resources"]:
		resources.write(el + "\n")

	# output to the configuration files in the tmp folder
	# url=$(<tmp/git_url.txt)

	# tests=$(<tmp/fetch_test.txt)
	# packages=$(<tmp/fetch_production.txt)
# /fetch_production.txt)

if __name__=='__main__':
	split_configuration()