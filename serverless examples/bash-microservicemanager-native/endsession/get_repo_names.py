import requests
import json
import simplejson

import sys
#4: provide an authenticated repository list and place them in a 
#	new file
payload = {'query': json.dumps({"tags":["test1", "test2"]})}
# arguments:
	# 0: this file name 
	# 1: username 
	# 2: password 
	

#this file will read into the configuration file for the url and whether to include
#tests or not. Either you get all packages+unit tests or just packages  

url='https://api.bitbucket.org/1.0/user/repositories'
# this could be trying to get the access token
# url = 'https://bitbucket.org/site/oauth2/authorize'


username = sys.argv[1]
password = sys.argv[2]

r = requests.get(url, auth=(username, password))
# r = {}
# r.te
# load the configuration file object 
config = json.loads(open("config.json", "r").read())

# prepare the template for the resources
config["resources"] = []

# url=$(<tmp/git_url.txt)
# tests=$(<tmp/fetch_test.txt)
# packages=$(<tmp/fetch_production.txt)

if __name__=='__main__':

	repository_list = json.loads(r.text)
	# for repo in repository_list:
	# 	name = repo['name']
	# 	resource_uri = repo['resource_uri']
	# 	config["resources"].append(name)

	    
	with open('config_dynamic.json', 'w') as outfile:
		# json.dump(config, outfile)
		print "Hello World"
		json.dump(config, outfile, indent=4, sort_keys=True)
		# outfile.write(simplejson.dumps(simplejson.loads(output), indent=4, sort_keys=True))
	# write the config object to the dynamic file type

	# dynamic_config.write(name+"\n")
