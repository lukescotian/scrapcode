    
from pymongo import MongoClient
from random import randint
#Step 1: Connect to MongoDB - Note: Change connection string as needed
client = MongoClient('mongodb+srv://lukescotian:4h7jksR123!@cluster0-qtu9s.mongodb.net/test?retryWrites=true')
# client=  MongoClient("mongodb://kxjfgpuduoxcy801:kxjfgpuduoxcy801@cluster0-shard-00-00-8zgqm.mongodb.net:27017,cluster0-shard-00-01-8zgqm.mongodb.net:27017,cluster0-shard-00-02-8zgqm.mongodb.net:27017/<DATABASE>?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin")
# mongodb://<USERNAME>:<PASSWORD>@cluster0-shard-00-00-8zgqm.mongodb.net:27017,cluster0-shard-00-01-8zgqm.mongodb.net:27017,cluster0-shard-00-02-8zgqm.mongodb.net:27017/<DATABASE>?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin
db=client.business
#Step 2: Create sample data
names = ['Kitchen','Animal','State', 'Tastey', 'Big','City','Fish', 'Pizza','Goat', 'Salty','Sandwich','Lazy', 'Fun']
company_type = ['LLC','Inc','Company','Corporation']
company_cuisine = ['Pizza', 'Bar Food', 'Fast Food', 'Italian', 'Mexican', 'American', 'Sushi Bar', 'Vegetarian']

# get the records
# print the records
def create(email):
	model = {
		"email" : email
	}
	result=db.reviews.insert_one(model)

def update(email):
	result = db.reviews.update_one({'email' : email }, {'$inc': {'likes': 2}})
	print result
# def delete():

def get(email):
	cursor = db.reviews.find({"email":email})
	for document in cursor:
	      print(document)

def list():
	cursor = db.reviews.find({})
	for document in cursor:
		print(document)

# create("lukescotian@outlook.com")
# print get("lukescotian@outlook.com")
print update("lukescotian@outlook.com")
