import moment from 'moment';
import $ from 'jquery';
require('imports-loader?$=jquery!../../node_modules/jquery-backstretch/jquery.backstretch.min.js');

function greet(){
    $('.other').backstretch([
      "assets/media/3b48ea0f.jpg",
      "assets/media/64b88398.jpg"
    ], {
        duration: 4000,
        transition: 'push_left|push_right|cover_up|cover_down|fade',
        transitionDuration: 250
    });
}
export default greet;