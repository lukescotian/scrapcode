# My own little technical specification
# []=array element <>class object {}hashmap key entry 
#make tree like structure
#				root(1)
#	   			  |
#	  			  V
#		_____[group_catagory](2)________
# 		|	   		|	   			|	 
#	  <PC> 		 <eyes>(3) 		<borough>
# 	  				|	    |
#     				V       V
#  			{k:'blue'|k:green}(4)
#     			 |
#     			 V
#   	[<INT>id=1,2,..n](5) or {<>id=1,2,..n}(5) or [<TREE>id=1,2,..n](5)
# using hashmaps for complexity because (1) is the shortest lookup time

the code speaks for itself but here below is a bit more detail on the implementation strategy.


the whole concept revolves around this pseucode 

to create tree***
for each record 
give id 
create subgroups sequentially
("dominant_color":{})
("dominant_color":{eyes:{}})
("dominant_color":{eyes:{brown:{}}}})

take id and put in subgroup hashmap as True 
("dominant_color":{eyes:{brown:{"id_uui123":True}}}})

repeat for each one, where each catagory stores all IDS
{dominant_color: {"brown": {id_123:True, id_234:True}}}
then
{gender: {"m": {id_123:True, id_237:True}}}

again, only saving ids, it is a reverse lookup table with speed 
GN*(1)*M
NG=min group number
M=number of other params to search (i.e. subgroups)

which does not really matter because those are in (1) time 

making the lookup (1) and the table creation N

I was going to use a tree to make it ln(n) but this was less code and faster
if it gets big each group can only save IDS into files, and lookup just the ids later


to search tree***
min_groupsize
for each group
	if groupsize < min_groupsize:
		push to end stack
	else 
		push start stack
smallest_group = stack.pop() //smallest
for each id in smallestgroup (dom)
