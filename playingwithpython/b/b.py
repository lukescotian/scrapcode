import re
import sys
import os.path
import tornado.httpserver
import webbrowser
import personalfile
import tabular as tb
import csv

try:
    import tornado.ioloop
    import tornado.web
except:
    print 'No tornado module installed, Please install the tornado from http://www.tornadoweb.org/'
    sys.exit()

SRC = os.path.dirname(__file__)

# Main Handler
class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("static/template_upload.html")



class OutputHandler(tornado.web.RequestHandler):
    def post(self):
        template_file = self.get_argument('template_html', None)
        **data_file = self.get_argument('csvfile', None)
        data_file = csv.DictReader(data_file)**
        data_file = tb.tabarray(data_file, verbosity=0, headerlines=1)
        xhtmlOutput = personalfile.function([templatehtmlfile,data_file])

        self.render('output.xhtml', data = xhtmlOutput)


application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/output", OutputHandler)
], debug=True,
   static_path=os.path.join(SRC, "static")
)



if __name__ == "__main__":
    print open(os.path.join(SRC, 'static', 'intro.txt')).read()
    webbrowser.open('http://127.0.0.1:8888')

    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
