import tornado.ioloop
import tornado.web
import os

print "If at any point you wish to quit the program hit Ctrl + C"

filetype = raw_input("What kind of file would you like to convert? Audio, Image, Video or Document: ")

if filetype == "Document":
   path = raw_input("Please drag and drop the directory in which the file is stored into the terminal:")
   os.chdir(path[1:-2])
   filename = raw_input("Please enter the name of the file you would like to convert, including the file-type. e.g. test.txt, however please do make sure that the file-name does not have any spaces:")
   Fileextension = raw_input("What filetype would you like the program to convert your file to. E.g. txt: ")
   from subprocess import check_call
   check_call(["unoconv", "-f", Fileextension, filename])

elif filetype == "Audio":
   path = raw_input("Please drag and drop the directory in which the file is stored into the terminal:")
   os.chdir(path[1:-2])
   filename = raw_input("Please enter the name of the file you would like to convert, including the file-type. e.g. test.txt, however please do make sure that the file-name does not have any spaces:")
   Fileextension = raw_input("What filetype would you like the program to convert your file to. E.g. mp3: ")
   body, ext = os.path.splitext("filename")
   from subprocess import check_call
   check_call(["ffmpeg" ,"-i", filename, body + "." + Fileextension])

elif filetype == "Video":
   path = raw_input("Please drag and drop the directory in which the file is stored into the terminal:")
   os.chdir(path[1:-2])
   filename = raw_input("Please enter the name of the file you would like to convert, including the file-type. e.g. test.txt, however please do make sure that the file-name does not have any spaces:")
   Fileextension = raw_input("What filetype would you like the program to convert your file to. E.g. mp4: ")
   body, ext = os.path.splitext("filename")
   from subprocess import check_call
   check_call(["ffmpeg" ,"-i", filename, body + "." + Fileextension])

elif filetype == "Image":
   path = raw_input("Please drag and drop the directory in which the file is stored into the terminal:")
   os.chdir(path[1:-2])
   filename = raw_input("Please enter the name of the file you would like to convert, including the file-type. e.g. test.txt, however please do make sure that the file-name does not have any spaces:")
   Fileextension = raw_input("What filetype would you like the program to convert your file to. E.g. JPG: ")
   body, ext = os.path.splitext("filename")
   from subprocess import check_call    
   check_call(["ffmpeg" ,"-i", filename, body + "." + Fileextension])

if __name__ == "__main__":
    application.listen(7777)
try:
    tornado.ioloop.IOLoop.instance().start()
except KeyboardInterrupt:
    tornado.ioloop.IOLoop.instance().stop()
