import tornado.web
import tornado.escape
import tornado.ioloop
import tornado.web
import json
from DogApi.DogAPI import DogAPI
import urlparse

class GetHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        # super(GetHandler, self).set_default_headers()

        # self.set_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))
        # self.set_header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS')
        # self.set_header('Access-Control-Allow-Credentials', 'true')
        # self.set_header('Access-Control-Allow-Headers', ','.join(
        #     self.request.headers.get('Access-Control-Request-Headers', '').split(',') +
        #     ['Content-Type']
        # ))
        self.set_header('Content-Type', 'application/json')

    def get(self):
        params={}
        count=0
        params = dict(urlparse.parse_qsl(urlparse.urlparse(self.request.uri).query))
        key_count=len(params.keys())
        if key_count==1:
            res=d.count_single(params.keys()[0], params[params.keys()[0]])
        elif key_count>1:   
            res=d.count_multiple(params)
        if "unknown fields" in res:
            self.clear()
            self.set_status(400)
        self.write(res)

def make_app():
    return tornado.web.Application([
        (r"/count", GetHandler),
    ])

if __name__ == "__main__":
    d=DogAPI("DogApi/data.csv")
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()