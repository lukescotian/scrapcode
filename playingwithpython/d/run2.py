
# An example tornado request handler that handles both JSON POST request
# bodies and x-www-form-urlencoded POST bodies.
#
# The benefit of JSON request bodies are more complicated and potentially
# nested dict and list data types.
#
# One drawback to JSON request bodies is that arguments can come in
# different types, so handlers will need to perform additional checks.
# With x-www-form-urlencoded fields, all argument values are strings, if
# they exist.

import tornado.escape
import tornado.ioloop
import tornado.web
import json
from DogApi.DogAPI import DogAPI

class JSONBaseHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        super(JSONBaseHandler, self).set_default_headers()

        self.set_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))
        self.set_header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Headers', ','.join(
            self.request.headers.get('Access-Control-Request-Headers', '').split(',') +
            ['Content-Type']
        ))

        self.set_header('Content-Type', 'application/json')
    def prepare(self):
        super(JSONBaseHandler, self).prepare()
        self.json_data = None
        if self.request.body:
            try:
                self.json_data = tornado.escape.json_decode(self.request.body)
            except ValueError:
                # TODO: handle the error
                pass

    def get_argument(self, arg, default=None):
        if self.request.method in ['POST', 'PUT'] and self.json_data:
            return self.json_data.get(arg, default)
        else:
            return super(JSONBaseHandler, self).get_argument(arg, default)

    def post(self):
        d=DogAPI("DogApi/data.csv")
        params={}
        count=0
        for i in self.request.arguments:
            params_=json.loads(i)
            for i in params_:
                if i not in params:
                    params[i]=params_[i]
        key_count=len(params.keys())
        if key_count==1:
            self.write(d.count_single(params.keys()[0], params[params.keys()[0]]))
        elif key_count>1:   
            self.write(d.count_multiple(params))
    def get(self):
        d=DogAPI("DogApi/data.csv")
        params={}
        count=0
        for i in self.request.arguments:
            params_=json.loads(i)
            for i in params_:
                if i not in params:
                    params[i]=params_[i]
        key_count=len(params.keys())
        if key_count==1:
            self.write(d.count_single(params.keys()[0], params[params.keys()[0]]))
        elif key_count>1:   
            self.write(d.count_multiple(params))
def make_app():
    return tornado.web.Application([
        (r"/", JSONBaseHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()

