import tornado.ioloop
import tornado.web
from DogApi.DogAPI import DogAPI
import json

class CountHandler(tornado.web.RequestHandler):
    def post(self):
        data = json.dumps({ k: self.get_argument(k) for k in self.request.arguments })
        print data
        DogAPI("data.csv")
        self.write(data)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Count world\n")

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/count", CountHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
