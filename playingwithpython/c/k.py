import csv
import uuid
db=open("data.csv")
i,rt,nc,fl=(0,{},{},db.readline())
for el in fl.strip('\n').split(','):
	nc[i],i=(el,i+1)
	rt[el]={}

csv_file= open('data.csv')
i,id__=(0,uuid.uuid4())
csv_reader = csv.reader(csv_file, delimiter=',')
flag = False
for line in csv_reader:
	if not flag:
		flag = True
		continue
	if i>=len(nc):
		continue
	i = 0
	for key in line:
		key = key.lower()
		if i>=len(nc):
			break
		if nc[i] in rt:
			if key in rt[nc[i]]:
				rt[nc[i]][key][id__]=True
			else :
				rt[nc[i]][key]={id__:True}
		i=i+1
def find_length(rt, param, el):
	el =el.lower()
	if param in rt and el.lower in rt[param]:
		return len(rt[param][el])
	else :
		return "Not there"

def find_length_multiple_params(rt,param_list):
	for el in param_list:
		for key in el:
			print(find_length(rt,key,el[key]))

param_list=[{"gender":"m"},
			{"borough":"brooklyn"},
			{"spayed_or_neutered":"no"}]
print find_length_multiple_params(rt,param_list)
print rt