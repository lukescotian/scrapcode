import csv
import uuid

#Written By Luke Morrison
#January 6th 2019
class DogAPI:
	def __init__(self,filename):
		# private variables 
		self.rt={}
		self.nc={}
		# public by __init__
		self.read(filename)

	def read(self,filename):	
		row_n=0
		with open(filename) as f:
		  reader = csv.reader(f)
		  for row in reader:
		  	if row_n==0:
			  	for el in row:
					self.nc[row_n],row_n=(el,row_n+1)
					self.rt[el]={}	
				row_n=row_n+1
				continue
			row_n,j,id__=(row_n+1,0,uuid.uuid4())
			for key in row:
				key = key.lower()
				if j>=len(self.nc):
					break
				if self.nc[j] in self.rt:
					if key in self.rt[self.nc[j]]:
						self.rt[self.nc[j]][key][id__]=True
					else :
						self.rt[self.nc[j]][key]={id__:True}
				j=j+1

	def count_single(self,param, element):
		return len(self.rt[param.lower()][element.lower()].keys())	

	def exists_in_tree(self,param,element):
		return param in self.rt and element in self.rt[param]

	def exists_in_group(self,param,element, id_):
		return param in self.rt and element in self.rt[param] and id_ in self.rt[param][element]

 	def retrieve(key,value):
		return self.rt[key][value]

	def count_multiple(self, p_obj):
		min_, stack,count=(9**9,[],0)
		#TODO remove c_group_size after evaluation is complete
		c_group_size=[]
		for c_key in p_obj:
			key,value=(c_key.lower(), p_obj[c_key.lower()].lower())
			if self.exists_in_tree(key,value) is False:
				return "Bad Error"
			elif self.count_single(key,value) < min_:
				stack.append((key,value))
				c_group_size.append(self.count_single(key,value)) #c_group_size is just to demonstrate the algo for recruiters
			else :
				stack.insert(0,(key,value))
				c_group_size.insert(0,self.count_single(key,value))
		#TODO - reduce 5 under line to a single lambda function with double map count return
		for r_id in (lambda id_tuple, map_: map_[id_tuple[0]][id_tuple[1]])((stack.pop()),self.rt):
			for i in range(0,len(stack)):
				k,v=stack[i]
				if self.exists_in_group(k,v,r_id):
					count=count+1
		return count

d=DogAPI("data.csv")
print d.count_multiple({'dominant_color': 'brindle',"gender":"f"})


