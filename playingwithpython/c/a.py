import tornado.ioloop
import tornado.web
# use case one - count by category listed at top 


class CountHandler(tornado.web.RequestHandler):
    def get(self):
    	a=''
        for i in open("data.csv"):
        	a=a+i
        self.write("Count world\n" + a)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Count world\n")

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/count", CountHandler),
])

if __name__ == "__main__":
    application.listen(8889)
    tornado.ioloop.IOLoop.instance().start()

If any invalid query string parameters are provided (i.e., parameters that do not correspond to column names in the CSV), the service should respond with a 400 status code
/count?fleas=no= where flies is unknown gives {"unknown fields": ["fleas"]}
a Content-Type header appropriate to the data in the response (i.e., application/json).