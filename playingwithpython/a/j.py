a =[2,-3,3,1,10,8,2,5,13,-5,3,-18]


def amplitude(thelist):
	max_=-99999999999999999
	for i in range(0,len(thelist)-1):
		ampl=abs(thelist[i+1]-thelist[i])

		if ampl>max_:
			max_=ampl
	return max_
	
seasons=["WINTER","SPRING","SUMMER","AUTUMN"]
max__=-9999999999
map_={}
from_,to_,step_=(0,len(a),len(a)/4)
for season in seasons:
	map_[season]=int(amplitude(a[from_:from_+step_]))
	from_=from_+step_
	
return max(map_, key=lambda k: map_[k])

