def helper_(char_map):
	flip_,str_,c, prev=({},'',0, None)
	while not all(value == 0 for value in char_map.values()):
		max_key=max(char_map, key=lambda k: char_map[k])
		if c>=2 and flip_[max_key]:
			key,cache_val=(max_key, char_map[max_key])
			del char_map[max_key]
			max_key=max(char_map, key=lambda k: char_map[k])
			char_map[key],c=(cache_val,-1)
		c,str_=(c+1,str_+max_key)
		if prev in flip_:
			flip_[prev]=False
		flip_[max_key]=True
		char_map[max_key], prev=(char_map[max_key]-1,max_key)	
	return str_
def solution(A, B):
    # write your code in Python 3.6
   	return helper_({"a":A,"b":B})

print solution(9,4)
#Basically the problem was given A=N B=M
