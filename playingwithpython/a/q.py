# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")

#Choose A as default to be dominant 
def solution(A, B):
    # write your code in Python 3.6
    str_=''
    counter=0
    flip=  {"a":True,"b":True}
    #Get max 
    while A>0 or B>0:
    	if A>B:
    		if counter >= 2 and flip["a"]:
    			str_=str_+"b"
    			counter=0
    			flip={"a":False,"b":True}
    			B=B-1
    		else:
    			str_=str_+"a"	
    			flip={"a":True,"b":False}
    			A=A-1
    			counter=counter+1
    	elif B>A:
    		if counter >= 2 and flip["b"]:
    			str_=str_+"a"
    			counter=0
    			flip={"a":True,"b":False}
    			A=A-1
    		else:
    			str_=str_+"b"	
    			flip={"a":False,"b":True}
    			B=B-1
    			counter=counter+1
    	else :
    		for key in flip:
    			if not flip[key]:
    				str_=str_ + key
    				if key == "a":
    					A=A-1
    					flip={"a":True,"b":False}
    				elif key == "b":
    					B=B-1
    					flip={"a":False,"b":True}
    				counter=counter+1
    			flip[key] = not flip[key]
    return str_
    			
print solution(9,4)
