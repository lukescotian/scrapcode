import unittest
from solution import get_neighbours, f, DialpadProblem

class TestGetNeighbours(unittest.TestCase):
    """
    Our basic test class
    """
    def test_getneighbours_normal(self):
        """
        Test non unique get neighbours method
        Test the most basic getneighours on the dialpad
        """
        sol = [5, 1, 7]
        res = get_neighbours([3,4], False)
        self.assertEqual(res, sol)


    def test_getneighbours_unique(self):
        """
        Test unique get neighbours method
        Test the most basic unique getneighbours
        """
        sol = [1, 5]
        res = get_neighbours([2,3,2,3,2], True)
        self.assertEqual(res, sol)

    def test_getneighbours_erroneousinput1(self):
        """
        Test getneighbours against string input
        The method should fail under string input
        """
        sol =  False
        res = get_neighbours("pizza", True)
        self.assertEqual(res, sol)

    def test_getneighbours_erroneousinput2(self):
        """
        Test getneighbours under string input
        Method should fail under string input 
        """
        sol =  False
        res = get_neighbours([2,3,2], "ss")
        self.assertEqual(res, sol)

    def test_getneighbours_outofboundsinput(self):
        """
        Test neighbours against outofboundsinput.
        The method shyould fail under outofboundsinput
        """
        sol =  False
        res = get_neighbours([2,3,2,3,200], True)
        self.assertEqual(res, sol)


class TestDialPadSolution(unittest.TestCase):
    """
    Our basic test class
    """

    def test_basicusecase_normal1(self):
        """
        Test dialpad against basicusecase methods
        standard basicuusecasemethods should basic pass  
        """
        start, length = 5,1
        sol = [5]
        res = f(start,length)
        self.assertEqual(res, sol)

    def test_basicusecase_normal2(self):
        """
        test the basic case from the problem descreiption
        """
        start, length = 1,3
        sol = [121, 123, 125, 145, 141, 147]
        res = f(start,length)
        self.assertEqual(res, sol)

    def test_basicusecase_normal3_unique(self):
        """
        Test when it is unique number 
        Test against when the input requires only unique numbers 
        """
        start, length = 1, 4
        sol = [1236, 1254, 1256, 1258, 1456, 1452, 1458, 1478]
        dpr = DialpadProblem(True)
        res = dpr.permute_dialpad([1],1,4,[])
        self.assertEqual(res, sol)

    def test_basicusecase_zerocase(self):
        """
        Test when the input is zero
        ensure the zero case is handled as a non importnat leading zero
        """
        start, length = 0,4
        sol = [878, 874, 898, 896, 854, 856, 852, 858]
        res = f(start,length)
        self.assertEqual(res, sol)

    def test_basicusecase_erroneousinput1(self):
        """
        test against the basic use case of erroneous input , in this case string  
        Test the dialpad problem against a string and expect a false
        """
        start, length = 1,'THEEND'
        sol = False
        res = f(start,length)
        self.assertEqual(res, sol)

    def test_basicusecase_erroneousinput2(self):
        """
        test against the basic use case of erroneous input , in this case dictioanary
        test dialpad against a dictionary and expect a false 
        """
        start, length = {}, 4
        sol = False
        res = f(start,length)
        self.assertEqual(res, sol)

    def test_basicusecase_outofboundsinput_low(self):
        """
        Test against negative input and expect false ..
        """
        start, length = -1, 4
        sol = False
        res = f(start,length)
        self.assertEqual(res, sol)

    def test_basicusecase_outofboundsinput_high(self):
        """
        test against super high input and expect false 
        the program truncates results because the callstack gets high (in the millions)
        """
        start, length = 1, 11
        sol = False
        res = f(start,length)
        self.assertEqual(res, sol)

unittest.main(exit=False)
	