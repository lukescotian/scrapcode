functionDomain-language_or_framework-triggers_or_services(max2 or *)-function_name-region_of_deployment

Break this down one time 

	functionDomain: lambda, bucket or whitelabel (currently)...
	language_or_framework: nodejs, python, reactjs, angularjs, java perhaps...
	triggers_or_services(max2 or *): http, smpt, mongo, mysql, etc..
	function_name: this_thing_does_that
	region_of_deployment: the fully qualified region name 

What is the function domain - bucket, whitelabel or lambda function?
What language or framework is the function in - python, nodejs, reactjs?
What triggers should the function have or services should the functions use -
	 http, cron, mongo, other service types..
	 or 
	 web, images, other storage types..?
What is the function name - Anything works :
Where is the region of deployment - currently frankfurt or ireland:
