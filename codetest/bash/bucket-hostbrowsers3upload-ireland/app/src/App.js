import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import { Link, Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <BrowserRouter>

        <div className="App">
            <li><Link to="/login">Log In</Link></li>

        </div>
      </BrowserRouter>
    );
  }
}

export default App;
