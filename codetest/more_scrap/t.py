


class Solution:

	def __init__(self, W,H,N):
		self.W = W
		self.H = H
		self.N = N
		self.dp = {}
		self.MAX_INT=999999999
		self.dx = [1,-1,0,0]
		self.dy = [0,0,-1,1]
		
	def calc(self,i,j):
		if (self.W<=self.H):
			return i + self.W * j
		return self.H * i + j

	def get (self,bitmask, i, j):
		return (bitmask & (1<<self.calc(i,j)))

	def bfs(self,bitmask):
		dist = [[]]
		maxDist = 0
		Q = []
		for i in range (0,self.W):
			for j in range (0,self.H):
				if (self.get(bitmask,i,j)):
					dist[i][j] = 0
					Q.append((i,k))
		while len(Q) > 0:
			(x,y) = Q[0]
			maxDist = max(maxDist, dist[x][y])
			del Q[0]
			for d in range(0,4):
				newx = x + self.dx[d]
				newy = y + self.dy[d]
				
				if newx>=self.W or newy >=self.H or newx < 0 or newy < 0:
					continue
				if dist[newx][newy] == -1:
					dist[newx][newy] = dist[x][y] + 1
					Q.append((newx,newy))
		return maxDist

	def getN(self):
		return self.N
	def solve (self, bitmask, left):
		left = self.N
		if left == 0:
			return self.bfs(bitmask)
		if (bitmask, left) in self.dp:
			return dp[(bitmask,left)]
		minDistance=self.MAX_INT
		for i in range(0,self.W):
			for j in range(0,self.H):
				if not self.get(bitmask,i,j):
					val = self.solve((bitmask | (1<<self.calc(i,j))), left-1)
					minDistance=min(minDistance,val)
		return minDistance
	
	
def findMinDistance(w, h, n):
	s = Solution(w,h,n)
	print s.solve(0, s.getN())
findMinDistance(4,4,3)
