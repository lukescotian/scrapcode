import scipy.interpolate

m = [40.0, 41.0, 40.0, 45.0]
t1 = [0.0, 1.1, 1.9, 3.0]
t2 = [0.4, 0.7, 1.8, 2.9, 3.3]
# return the one-dimensional interpolated values of m at t2.

def get_delta_t(time_series):
    start = time_series[0]
    ret=[]
    for i in range(1, len(time_series)-1):
        ret.append(abs(start-time_series[i]))
    return ret

def interpolated_values_of_m(measurements, timepoints):
    # are we interpolating x2+y2
    # based on m_n,(delta_t_n)
    ret = []
    timepoints = get_delta_t(timepoints)
    # if cases
    # interpolation between two delta 
    # case1 same length array?
    # if (len(measurements) == len(timepoints)):
    # assume index is same measurements has => m...n, where we can always take more timepoints later on
    # i.e. timepoints=>m...n....o...p...etc
    if (len(measurements) > len(timepoints)):
        min = len(measurements)
    else:
        min = len(timepoints)
    for i in range(0,min-1):
        y_interp=scipy.interpolate.interp1d(measurements[i],timepoints[i])
        ret.append(y_interp)
    return ret
