import unittest
from neighborobj import nobj
from permojb import pobj


class TestFactorial(unittest.TestCase):
    """
    Our basic test class
    """

    def test_fact(self):
        """
        The actual test.
        Any method which starts with ``test_`` will considered as a test case.
        """
        sample_value = 5
        res = fact(5)
        self.assertEqual(res, 120)


if __name__ == '__main__':
	unittest.main()