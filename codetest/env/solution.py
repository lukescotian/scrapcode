
"""
Application - To Answer the dialer problem
Usage:
  python solution.py 
  python test_solution.py
Options:
  -h, --help    Show this screen.
  --version     Show version.
  --arg2 <key>  Another argument [default: 42].
"""

__author__ = 'Luke Morrison'
__license__ = 'MIT'
__version__ = '0.0.1'
__maintainer__ = 'Luke Morrison'
__email__ = 'lukescotian@outlook.com'
__status__ = 'Development'


number = -1
def verify_variable(number):
	"""
	Verify the variable is an integer within the correct domain 0-9
	@param number: an integer between 0 and 9 
	ret: whether or not it is a validated integer 
	"""
	if isinstance(number, int) is False:
		return False
	try:
	    number = int(number)
	    if number < 0:  # if not a positive int print message and ask for input again
	        return False
	    if number > 9:
	    	return False
	except ValueError:
	    return False
	return True 

def get_node_neighbours(a, last_value, isUnique):
	"""
	Provide the neighbours of this digit
	@param a - list of numbers that have been already appended to the list
	@param last_value - an integer between 0 and 9 most recently appended
	@param isUnique bool to determine if it is unique
	ret: an array of interegers that are neighboring digits  
	"""

	neighbours = []
	if last_value is False or last_value > 9 or last_value < 0 or a == False:
		return False
	if last_value == 0:
		return [8]
	if last_value % 3 is not 1 and last_value < 10 and last_value > 0:
		v = last_value-1
		if isUnique == False:
			neighbours.append(v)
		elif v not in a:
			neighbours.append(v)
	if last_value % 3 is not 0 and last_value < 10 and last_value > 0:
		v = last_value+1
		if isUnique == False:
			neighbours.append(v)
		elif v not in a:
			neighbours.append(v)
	if last_value > 3 and last_value < 10:
		v = last_value-3
		if isUnique == False:
			neighbours.append(v)
		elif v not in a:
			neighbours.append(v)
	if last_value < 7 and last_value > 0:
		v = last_value+3
		if isUnique == False:
			neighbours.append(v)
		elif v not in a:
			neighbours.append(v)
	return neighbours


def get_neighbours(current_numberlist, isUnique):
	"""
	Provide the neighbours of this digit
	@param a - list of numbers that have been already appended to the list
	@param last_value - an integer between 0 and 9 most recently appended
	@param isUnique bool to determine if it is unique
	ret: an array of interegers that are neighboring digits  
	"""
	if isinstance(isUnique, bool) == False:
		return False
	last_value = current_numberlist[len(current_numberlist)-1]
	return get_node_neighbours(current_numberlist, last_value, isUnique)

class DialpadProblem:
	"""
	A class to computer the dialpad problem as described in the problem description 

	ret: an array of interegers that answer the problem
	"""
	def __init__(self, isUnique):
		self.isUnique = isUnique

	def permute_dialpad(self, curr_array, start, length, ret):
		"""
		Create a permutation fo the dialpad using backtrack recursion 
		curr_array - an array being backtracked, starting with a single initial value 
		start - a start value for this problem
		length - how long the end integers should be  
		ret: an array of interegers that answer the problem
		"""
		if length == 0:
			return []

		if len(curr_array) == length:
			ret.append(int("".join(str(x) for x in curr_array)))
			return ret

		neighbours = get_neighbours(curr_array, self.isUnique)

		for i in neighbours:
			curr_array.append(i)
			self.permute_dialpad(curr_array, start, length, ret)
			curr_array.pop() #backtrack
		return ret

def f(start_index,length):
	"""
	Subcall method to answer the dialpad problem
	start_index - the starting index to this problem
	length - how long the end integers should be  
	ret: an array of interegers that answer the problem
	"""

	if verify_variable(start_index) is False:
		return False
	if verify_variable(length) is False:
		return False
	dpr = DialpadProblem(False)
	ret = dpr.permute_dialpad([start_index],1,length,[])
	return ret
# assumption - since the answer wants 
# a NUMBER - it will get integers
# by default the integers will remove
# leading zeros i.e. 002
# therefore starting from 0 will be equal to lenght-1 and starting from 8 
print f(1,5)
