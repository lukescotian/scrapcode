
def get_node_neighbours(a, last_value):
	neighbours = []
	if last_value == 0:
		return [8]
	if last_value % 3 is not 1 and last_value < 10 and last_value > 0:
		v = last_value-1
		if v not in a:
			neighbours.append(v)
	if last_value % 3 is not 0 and last_value < 10 and last_value > 0:
		v = last_value+1
		if v not in a:
			neighbours.append(v)
	if last_value > 3 and last_value < 10:
		v = last_value-3
		if v not in a:
			neighbours.append(v)
	if last_value < 7 and last_value > 0:
		v = last_value+3
		if v not in a:
			neighbours.append(v)
	return neighbours

def get_neighbours(a):
	last_value = a[len(a)-1]
	return get_node_neighbours(a, last_value)

def permute_dialpad(curr_array, start, length, ret):

	if length == 0:
		return []

	if len(curr_array) == length:
		ret.append(int("".join(str(x) for x in curr_array)))
		return ret
	neighbours = get_neighbours(curr_array)

	for i in neighbours:
		curr_array.append(i)
		permute_dialpad(curr_array, start, length, ret)
		curr_array.pop()
	return ret


a= f(1,3)
print "Here is a"
print a